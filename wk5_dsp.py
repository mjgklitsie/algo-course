__author__ = 'mjgklitsie'
from heapdict import *


def getgraph(string):
    graph = dict()

    import csv
    with open(string, newline='') as csvfile:
        file1 = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for line in file1:
            node = int(line[0])
            graph[node] = []
            npos = len(line)
            for m in range(1, npos):
                if line[m]:
                    s = line[m].split(',')
                    graph[node].append([int(s[0]), int(s[1])])

    return graph


def dsp(graph):
    # Import array as dict vertex-from as keys and tuples (vertex-to, distance, dijkstra-distance as 1M)
    # Create an empty heapdict with vertex-from as keys and 1.000.000 as dijkstra-distances
    # Set vertex 0 to 0 in heapdict and dict
    # Pop, check, change, repeat!

    # Creating initial conditions
    dist = [1000000] * (len(graph) + 1)
    heap = heapdict()
    for nvert in range(1, len(graph)+1):
        heap[nvert] = 1000000
    heap[1] = 0

    # Dijkstra's loop
    while len(heap) != 0:
        key, ddist = heap.popitem()
        dist[key] = ddist
        values = graph[key]
        for val in values:
            if val[0] in heap:
                tmp = heap[val[0]]
                if heap[val[0]] > ddist + val[1]:
                    heap[val[0]] = ddist + val[1]

    return dist

x = getgraph('arrays/wk5_array.txt')
dist = dsp(x)
print(dist[7],dist[37],dist[59],dist[82],dist[99],dist[115],dist[133],dist[165],dist[188],dist[197])