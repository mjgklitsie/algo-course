__author__ = 'mjgklitsie'
from copy import deepcopy
from math import log10
import time


def getgraph(string):
    graph = dict()
    import csv
    with open(string, newline='') as csvfile:
        file1 = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for line in file1:
            node = int(line[0])
            edges = []
            for edge in line[1:len(line)]:
                if edge:
                    edges.append(int(edge))
            graph[node] = edges
    return graph


def contraction(graph, cuts):  # 'Cheats' while determining random choice of vertices.
    from random import choice
    start = choice(list(graph.keys()))

    while len(graph) > 2:
        start = choice(list(graph.keys()))
        finish = choice(list(graph[start]))

        if len(graph[start]) < len(graph[finish]):
            start, finish = finish, start

        # Adding the edges from the absorbed node:
        # Deleting the references to the adsorbed node and changing to the source node:
        for edge in graph[finish]:
            graph[edge].remove(finish)
            if edge != start:
                graph[edge].append(start)
                graph[start].append(edge)
        del graph[finish]

    mincut = len(graph[start])
    cuts.append(mincut)

# Getting the graph
graph0 = getgraph('arrays\wk3_array.txt')
# How many times should contraction run? (N^2*logN)
irun = int(log10(len(graph0)) * len(graph0) * len(graph0))

cuts0 = []

starttime = time.process_time()
for i in range(1, irun):
    graph1 = deepcopy(graph0)
    contraction(graph1, cuts0)
    if len(cuts0) % 100 == 0:
        print('after {:d} iterations and {:.0f} seconds, min cut: {:d}'.format(i, time.process_time()-starttime,min(cuts0)))

print('Min cut is ', min(cuts0))