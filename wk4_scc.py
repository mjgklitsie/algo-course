__author__ = 'mjgklitsie'
from collections import deque


def getgraph(string):
    graph = dict()
    rgraph = dict()

    import csv
    with open(string, newline='') as csvfile:
        file1 = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for line in file1:
            node = int(line[0])
            edge = int(line[1])
            if node in graph:  # forward graph
                graph[node].append(edge)
            else:
                graph[node] = [edge]
            if edge in rgraph:  # reverse graph
                rgraph[edge].append(node)
            else:
                rgraph[edge] = [node]

    return graph, rgraph


def dfs(graph, order, graphtype):
    # Setting up
    n = len(order)  # Number of nodes
    explore = [0]*(n+1)  # Checklist of (un)explored vertices/nodes

    run_order = deque([])  # Setting up stack for running order 2nd pass of DFS
    leaderstack = [0, 0, 0, 0, 0]  # Setting up leaderboard for best 5 (+ 1) SCC's
    nitem = 0

    while len(order) > 0:  # Order of looping through nodes
        leader = 0
        nkey = order.pop()
        if explore[nkey] == 0:
            explore[nkey] = 1
            stack = deque([nkey])
            while len(stack) > 0:
                if nkey not in graph or nitem >= len(graph[nkey]):  # If node has no outgoing edges; all are discovered
                    stack.pop()  # Remove node from stack
                    nitem = 0  # Reset counter for edges
                    run_order.append(nkey)  # Add node to order for 2nd pass
                    leader += 1  # Add increase counter for leaderboard
                    if len(stack) != 0:  # Necessary when stack is empty
                        nkey = stack[-1]
                else:
                    node = graph[nkey][nitem]
                    if explore[node] == 0:
                        explore[node] = 1
                        stack.append(node)
                        nitem = 0
                        nkey = node
                    else:
                        nitem += 1  # Increase counter for outgoing edges
        if leader > leaderstack[0]:  # If counter for leaderboard is large enough: add and sort.
            leaderstack.pop(0)
            leaderstack.append(leader)
            leaderstack.sort()

    if graphtype == 0:
        return run_order
    elif graphtype == 1:
        return leaderstack
    else:
        return 0

# Getting general information about graphs
graph1, rgraph1 = getgraph('wk4_array.txt')
maxnode = max(max(graph1.keys()), max(rgraph1.keys()))
rev_order = deque(list(range(1, maxnode+1)))
print('Grootst gevonden node:', maxnode)

# Running DFS in reverse graph to get reordering of nodes
fwd_order = dfs(rgraph1, rev_order, 0)
print('Aantal nodes in fwdorder:', len(fwd_order))

# Running DFS to get 5 largest SCC's
leader1 = dfs(graph1, fwd_order, 1)
print('Leader:', leader1)