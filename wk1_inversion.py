__author__ = 'mjgklitsie'
import time


def getfile(string):
    #Create list of integers
    lst = []
    import csv
    with open(string, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            lst.append(int(row[0]))
    return lst


def inversion(lst_unsort):
    if len(lst_unsort) == 1:
        count_inv = 0
        lst_sort = lst_unsort
        return lst_sort, count_inv
    else:
        m = len(lst_unsort)
        if m % 2 == 1:
            n = (m + 1) // 2
        else:
            n = m // 2
        lst1_unsort = lst_unsort[:n]
        lst2_unsort = lst_unsort[n:m]
        lst1_sort, x = inversion(lst1_unsort)
        lst2_sort, y = inversion(lst2_unsort)
        lst_sort, z = split(lst1_sort, lst2_sort)
        count_inv = x + y + z
        return lst_sort, count_inv


def split(list1_unsort, list2_unsort):
    i = 0
    j = 0
    i_check = 0
    count_inv = 0

    n_list1 = len(list1_unsort)
    n_list2 = len(list2_unsort)
    n_tot = n_list1 + n_list2

    lst_sort = []

    for k in range(n_tot):
        if i_check == 0:
            if list1_unsort[i] < list2_unsort[j]:
                lst_sort.append(list1_unsort[i])
                if i != n_list1-1:
                    i += 1
                else:
                    i_check = 1
            else:
                lst_sort.append(list2_unsort[j])
                count_inv = count_inv + n_list1 - i
                if j != n_list2 - 1:
                    j += 1
                else:
                    i_check = 2
        elif i_check == 1:
            lst_sort.append(list2_unsort[j])
            j += 1
        elif i_check == 2:
            lst_sort.append(list1_unsort[i])
            i += 1

    return lst_sort, count_inv


def inversionbrute(lst_unsort):
    count_inv = 0

    if len(lst_unsort) == 1:
        return count_inv

    n_list = len(lst_unsort)

    for i in range(n_list):
        for j in range(i, n_list):
            if lst_unsort[i] > lst_unsort[j]:
                count_inv += 1

    return count_inv

lst1 = getfile('arrays\wk1_array.txt')
lst2 = getfile('arrays\wk1_array.txt')

#time_start = time.process_time()
#ibrute = inversion.inversionbrute(lst1) #this takes about 20 min!
#timeibf = time.process_time()-time_start

time_start = time.process_time()
lst2sort, imerge = inversion(lst2)
timeimerge = time.process_time()-time_start

#print('Brute-Force: {:.3f} seconds, {:d} inversions'.format(timeibf, ibrute))
print('Merge: {:.3f} seconds, {:d} inversions'.format(timeimerge, imerge))