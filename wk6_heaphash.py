__author__ = 'mjgklitsie'
from bisect import *
from heapq import *


def getfile(string):
    #Create list of integers
    lstdict = dict()
    import csv
    with open(string, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            lstdict[int(row[0])] = 1
    lst1 = list(lstdict.keys())
    lst1.sort()
    return lst1


def getfile2(string):
    #Create list of integers
    lst2 = []
    import csv
    with open(string, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            lst2.append(int(row[0]))
    return lst2


def twosum(lst3):
    # Create list of target values of t
    tdict = dict()
    for val in lst3:
        # Determine range for y
        ymin = -10000 - val
        yplus = 10000 - val

        imin = bisect_left(lst3, ymin)
        iplus = bisect_right(lst3, yplus)

        for y in (lst3[p] for p in range(imin,iplus)):
            tdict[y+val]=1

    return len(tdict)


def summedian(lst4):
    # Setting up boundaries

    heapn = []  # This will be a max-heap There for negative values
    heapm = []  # This will be a min-heap
    median = 0

    if lst4[0] < lst4[1]:
        heapn.append(-lst4[0])
        heapm.append(lst4[1])
        median -= 2 * heapn[0]
    else:
        heapn.append(-lst4[1])
        heapm.append(lst4[0])
        median = lst4[0] + lst4[1]

    for k in range(2, len(lst4)):
        kval = lst4[k]
        if k%2 == 0:  # current number of values in heaps is even
            if kval > heapm[0]:
                heappush(heapm, kval)
                val = heappop(heapm)
                heappush(heapn, -val)  # negate value and add to maxheap
            else:
                heappush(heapn, -kval)  # negate value and add to maxheap
        else:  # current number of values in heaps is uneven
            if kval < -heapn[0]:
                heappush(heapn, -kval)  # negate value and add to maxheap
                val = heappop(heapn)
                heappush(heapm, -val)  # pop value and add negated value to minheap
            else:
                heappush(heapm, kval)
        median -= heapn[0]

    return median



lsta = getfile('arrays\wk6_sum.txt')
num = twosum(lsta)
print(num)

lstb = getfile2('arrays\wk6_median.txt')
num2 = summedian(lstb)
print(num2%10000)