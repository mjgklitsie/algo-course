__author__ = 'mjgklitsie'
import time


def getfile(string):
    #Create list of integers
    lst = []
    import csv
    with open(string, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            lst.append(int(row[0]))
    return lst


def qsort(lst, version):
    #Starting conditions
    ifirst = 0
    ilast = len(lst)-1

    #Sorting and counting
    if version == 'v1':
        icomp = sort_v1(lst, ifirst, ilast)
    elif version == 'v2':
        icomp = sort_v2(lst, ifirst, ilast)
    elif version == 'v3':
        icomp = sort_v3(lst, ifirst, ilast)
    elif version == 'brute':
        icomp = qsort_brute(lst)
    else:
        print('Error')
        return

    return icomp


def sort_v1(lst, ifirst, ilast):
    #BASECASE
    if ifirst >= ilast:
        icomp = 0
        return icomp

    #COUNT COMPARISONS
    x = ilast - ifirst

    #BEPALING PIVOT EN V2,3 REARRANGE!
    pivot = lst[ifirst]

    #SORTING
    i = ifirst + 1

    for k in range(ifirst+1, ilast+1):
        if lst[k] < pivot:
            lst[k], lst[i] = lst[i], lst[k]
            i += 1

    #PIVOT ON CORRECT SPOT
    lst[ifirst], lst[i-1] = lst[i-1], lst[ifirst]

    ileftfirst = ifirst
    ileftlast = i - 2
    irightfirst = i
    irightlast = ilast

    y = sort_v1(lst, ileftfirst, ileftlast)
    z = sort_v1(lst, irightfirst, irightlast)

    icomp = x + y + z

    return icomp


def sort_v2(lst, ifirst, ilast):
    #BASECASE
    if ifirst >= ilast:
        icomp = 0
        return icomp

    #COUNT COMPARISONS
    x = ilast - ifirst

    #BEPALING PIVOT EN V2,3 REARRANGE!
    lst[ifirst], lst[ilast] = lst[ilast], lst[ifirst]
    pivot = lst[ifirst]

    #SORTING
    i = ifirst + 1

    for k in range(ifirst+1, ilast+1):
        if lst[k] < pivot:
            lst[k], lst[i] = lst[i], lst[k]
            i += 1

    #PIVOT ON CORRECT SPOT
    lst[ifirst], lst[i-1] = lst[i-1], lst[ifirst]

    ileftfirst = ifirst
    ileftlast = i - 2
    irightfirst = i
    irightlast = ilast

    y = sort_v2(lst, ileftfirst, ileftlast)
    z = sort_v2(lst, irightfirst, irightlast)

    icomp = x + y + z

    return icomp


def sort_v3(lst, ifirst, ilast):
    #BASECASE
    if ifirst >= ilast:
        icomp = 0
        return icomp

    #COUNT COMPARISONS
    x = ilast - ifirst

    #BEPALING PIVOT EN V2,3 REARRANGE!
    len_array = ilast - ifirst + 1

    if len_array == 2:  # There is no middle pivot!
        if lst[ifirst] < lst[ilast]:
            return x
        else:
            lst[ifirst], lst[ilast] = lst[ilast], lst[ifirst]
            return x

    imiddle = (ilast - ifirst)//2 + ifirst

    if lst[ifirst] < lst[imiddle] and lst[ifirst] < lst[ilast]:
        if lst[imiddle] < lst[ilast]:
            pivot = lst[imiddle]
            lst[ifirst], lst[imiddle] = lst[imiddle], lst[ifirst]
        else:
            pivot = lst[ilast]
            lst[ifirst], lst[ilast] = lst[ilast], lst[ifirst]
    elif lst[ifirst] > lst[imiddle] and lst[ifirst] > lst[ilast]:
        if lst[imiddle] > lst[ilast]:
            pivot = lst[imiddle]
            lst[ifirst], lst[imiddle] = lst[imiddle], lst[ifirst]
        else:
            pivot = lst[ilast]
            lst[ifirst], lst[ilast] = lst[ilast], lst[ifirst]
    else:
        pivot = lst[ifirst]

#SORTING
    i = ifirst + 1

    for k in range(ifirst+1, ilast+1):
        if lst[k] < pivot:
            lst[k], lst[i] = lst[i], lst[k]
            i += 1

    #PIVOT ON CORRECT SPOT
    lst[ifirst], lst[i-1] = lst[i-1], lst[ifirst]

    ileftfirst = ifirst
    ileftlast = i - 2
    irightfirst = i
    irightlast = ilast

    y = sort_v3(lst, ileftfirst, ileftlast)
    z = sort_v3(lst, irightfirst, irightlast)

    icomp = x + y + z

    return icomp


def qsort_brute(lst):
    n = len(lst)

    for k in range(n-1):
        for j in range(k, n):
            if lst[k] > lst[j]:
                lst[k], lst[j] = lst[j], lst[k]

    icomp = (n*(n + 1))//2 - 1

    return icomp

lst1 = getfile('arrays\wk2_array.txt')
lst2 = getfile('arrays\wk2_array.txt')
lst3 = getfile('arrays\wk2_array.txt')
#lst4 = getfile('arrays\wk2_array.txt')

time_start = time.process_time()
icomp1 = qsort(lst1, 'v1')
time_qsortv1 = time.process_time()-time_start

time_start = time.process_time()
icomp2 = qsort(lst2, 'v2')
time_qsortv2 = time.process_time()-time_start

time_start = time.process_time()
icomp3 = qsort(lst3, 'v3')
time_qsortv3 = time.process_time()-time_start

#time_start = time.process_time()
#icompbrute = qsort(lst4, 'brute')
#time_qsortbrute = time.process_time()-time_start

print('version {:d}: {:.3f} seconds, {:d} comparisons'.format(1, time_qsortv1, icomp1))
print('version {:d}: {:.3f} seconds, {:d} comparisons'.format(2, time_qsortv2, icomp2))
print('version {:d}: {:.3f} seconds, {:d} comparisons'.format(3, time_qsortv3, icomp3))
#print('brute force: {:.3f} seconds sorting, {:d} comparisons'.format(time_qsortbrute, icompbrute))