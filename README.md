algo-course
===========

Stuff from learning python
- Divide and conquer algorithms:
  - Week1: inversion
  - Week2: quicksort
- Graph algorithms:
  - Week3: contraction
  - Week4: strongly connected components
  - Week5: Dijkstra's Shortest Distance
- Hashtables and heaps
  - Week6: Sum of medians, 2-SUM Algorithm
